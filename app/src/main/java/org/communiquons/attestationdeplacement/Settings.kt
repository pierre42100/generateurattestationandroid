package org.communiquons.attestationdeplacement

import android.content.Context
import androidx.preference.PreferenceManager

class Settings(
    var name: String,
    var dateOfBirth: String,
    var placeOfBirth: String,
    var address: String
) {

    companion object {

        val GENERATED_ATTESTATION_PDF: String = "generated_attestation.pdf"

        fun get(ctx: Context): Settings {
            val pref = PreferenceManager.getDefaultSharedPreferences(ctx)

            return Settings(
                pref.getString("name", null).orEmpty(),
                pref.getString("date_of_birth", null).orEmpty(),
                pref.getString("place_of_birth", null).orEmpty(),
                pref.getString("address", null).orEmpty()
            )
        }

        fun getSignatureLocation(ctx: Context): String {
            return PreferenceManager.getDefaultSharedPreferences(ctx)
                .getString("sig_location", "Paris")!!
        }

        fun setSignatureLocation(ctx: Context, loc: String) {
            PreferenceManager.getDefaultSharedPreferences(ctx).edit().putString("sig_location", loc)
                .apply();
        }
    }

    fun isValid(): Boolean {
        return name.isNotEmpty() &&
                dateOfBirth.isNotEmpty() &&
                placeOfBirth.isNotEmpty() &&
                address.isNotEmpty()
    }
}