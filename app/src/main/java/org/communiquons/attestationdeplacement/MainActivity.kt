package org.communiquons.attestationdeplacement

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ShareCompat
import androidx.core.content.FileProvider
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

        setContentView(R.layout.activity_main)

        if (!Settings.get(this).isValid())
            openSettings()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.action_create_attestation -> createAttestation()
            R.id.action_settings -> openSettings()
            R.id.action_share -> shareAttestation()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        refreshScreen()
    }

    private fun openSettings() {
        startActivity(Intent(this, SettingsActivity::class.java))
    }

    private fun createAttestation() {
        /*AlertDialog.Builder(this)
            .setTitle(R.string.dialog_choose_attestation_type_title)
            .setMessage(R.string.dialog_choose_attestation_type_message)
            .setNegativeButton(R.string.dialog_choose_attestation_type_couvre_feu) { _, _ -> startActivity(Intent(this, CreateAttestationCouvreFeuMars2021Activity::class.java))}
            .setPositiveButton(R.string.dialog_choose_attestation_type_confinement) { _, _ -> startActivity(Intent(this, CreateAttestationConfinementMars2021Activity::class.java))}
            .setNeutralButton(R.string.dialog_choose_attestation_type_old) { _, _ -> startActivity(Intent(this, CreateAttestationOct2020Activity::class.java))}
            .create()
            .show()*/

        startActivity(Intent(this, CreateAttestationCouvreFeuMai2021Activity::class.java))
    }

    private fun shareAttestation() {

        val dir = File(filesDir, "external")
        dir.mkdirs();

        val inFile = File(filesDir, Settings.GENERATED_ATTESTATION_PDF).readBytes()
        val outFile = File(filesDir, "external/attestation.pdf")
        outFile.writeBytes(inFile)


        val contentUri = FileProvider.getUriForFile(
            this,
            "org.communiquons.attestationdeplacement.provider",
            outFile
        )

        val intent = ShareCompat.IntentBuilder.from(this)
            .setStream(contentUri)
            .setType("application/pdf")
            .intent
            .setAction(Intent.ACTION_SEND)
            .setDataAndType(contentUri, "application/pdf")
            .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        startActivity(intent)
    }

    private fun refreshScreen() {
        val file = File(filesDir, Settings.GENERATED_ATTESTATION_PDF)

        if (!file.exists()) {
            pdfViewer.visibility = View.GONE
            return
        }

        pdfViewer.visibility = View.VISIBLE

        pdfViewer.fromFile(file)
            .enableSwipe(true)
            .enableDoubletap(true)
            .load()
    }
}