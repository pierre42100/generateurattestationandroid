package org.communiquons.attestationdeplacement


import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfContentByte
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfStamper
import kotlinx.android.synthetic.main.activity_nouvelle_attestation_couvre_feu_mars_2021.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*


class CreateAttestationCouvreFeuMai2021Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nouvelle_attestation_couvre_feu_mai_2021)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val calendar = Calendar.getInstance()
        val date =
            "${pad(calendar.get(Calendar.DAY_OF_MONTH))}/${pad(calendar.get(Calendar.MONTH) + 1)}/${pad(
                calendar.get(
                    Calendar.YEAR
                )
            )}"
        sig_date.setText(date)

        val time =
            "${pad(calendar.get(Calendar.HOUR_OF_DAY))}:${pad(calendar.get(Calendar.MINUTE))}"
        sig_time.setText(time)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_create_attestation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home)
            finish()

        if (item.itemId == R.id.action_create)
            create()

        return super.onOptionsItemSelected(item)
    }


    private fun writeText(page: PdfContentByte, text: String, x: Float, y: Float) {
        //Create BaseFont instance.
        val baseFont = BaseFont.createFont(
            BaseFont.TIMES_ROMAN,
            BaseFont.CP1252, BaseFont.NOT_EMBEDDED
        )

        page.beginText()
        page.setFontAndSize(baseFont, 14F)
        page.setTextMatrix(x, y)
        page.showText(text)
        page.endText()
    }

    // Based on https://www.w3spoint.com/itext-edit-add-modify-write-an-existing-pdf-file-in-java
    private fun create() {


        val settings = Settings.get(this)

        if (!settings.isValid())
            Toast.makeText(this, R.string.err_incomplete_settings, Toast.LENGTH_SHORT).show()

        // Get base PDF
        val attestation =
            assets.open("03-05-2021-attestation-de-deplacement-derogatoire.pdf").readBytes()

        val pdfReader = PdfReader(attestation)
        val out = ByteArrayOutputStream()
        val pdfStamper = PdfStamper(pdfReader, out)


        //Get the number of pages in pdf.
        val pages = pdfReader.numberOfPages
        if (pages != 1)
            throw RuntimeException("PDF has not one page!")

        // Get the first page
        val pageOneContentBytes = pdfStamper.getOverContent(1)

        // Full name
        writeText(pageOneContentBytes, settings.name, 130F, 630F)

        // Date of birth
        writeText(pageOneContentBytes, settings.dateOfBirth, 140F, 605F)

        // Address
        writeText(pageOneContentBytes, settings.address, 145F, 581F)


        // Reason
        if (workCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 529F, 470F)

        if (medicalCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 529F, 400F)

        if (familyCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 529F, 350F)

        if (administrativeCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 529F, 290F)


        if (trainCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 529F, 232F)

        if (petCheckBox.isChecked)
            writeText(pageOneContentBytes, "X", 529F, 179F)


        // Sig date
        writeText(pageOneContentBytes, sig_date.text.toString(), 350F, 90F)

        // Sig time
        writeText(pageOneContentBytes, sig_time.text.toString(), 445F, 90F)

        // Signature
        writeText(pageOneContentBytes, settings.name, 400F, 65F)


        pdfStamper.close()

        Log.v("CreateAttestation", "Done creating attestation.")

        val fileOut = FileOutputStream(File(filesDir, Settings.GENERATED_ATTESTATION_PDF))
        fileOut.write(out.toByteArray())
        fileOut.flush()
        fileOut.close()


        finish()
    }
}