package org.communiquons.attestationdeplacement

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfContentByte
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfStamper
import kotlinx.android.synthetic.main.activity_create_attestation_lockdown_mars_2021.*
import kotlinx.android.synthetic.main.activity_create_attestation_oct_2020.*
import kotlinx.android.synthetic.main.activity_create_attestation_oct_2020.administrativeCheckbox
import kotlinx.android.synthetic.main.activity_create_attestation_oct_2020.familyCheckbox
import kotlinx.android.synthetic.main.activity_create_attestation_oct_2020.handicapCheckbox
import kotlinx.android.synthetic.main.activity_create_attestation_oct_2020.medicalCheckbox
import kotlinx.android.synthetic.main.activity_create_attestation_oct_2020.sig_date
import kotlinx.android.synthetic.main.activity_create_attestation_oct_2020.sig_location
import kotlinx.android.synthetic.main.activity_create_attestation_oct_2020.sig_time
import kotlinx.android.synthetic.main.activity_create_attestation_oct_2020.trainCheckbox
import kotlinx.android.synthetic.main.activity_create_attestation_oct_2020.workCheckbox
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*


class CreateAttestationConfinementMars2021Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_attestation_lockdown_mars_2021)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Pre-fill field
        sig_location.setText(Settings.getSignatureLocation(this))

        val calendar = Calendar.getInstance()
        val date =
            "${pad(calendar.get(Calendar.DAY_OF_MONTH))}/${pad(calendar.get(Calendar.MONTH) + 1)}/${pad(
                calendar.get(
                    Calendar.YEAR
                )
            )}"
        sig_date.setText(date)

        val time =
            "${pad(calendar.get(Calendar.HOUR_OF_DAY))}:${pad(calendar.get(Calendar.MINUTE))}"
        sig_time.setText(time)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_create_attestation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home)
            finish()

        if (item.itemId == R.id.action_create)
            create()

        return super.onOptionsItemSelected(item)
    }


    private fun writeText(page: PdfContentByte, text: String, x: Float, y: Float) {
        //Create BaseFont instance.
        val baseFont = BaseFont.createFont(
            BaseFont.TIMES_ROMAN,
            BaseFont.CP1252, BaseFont.NOT_EMBEDDED
        )

        page.beginText()
        page.setFontAndSize(baseFont, 14F)
        page.setTextMatrix(x, y)
        page.showText(text)
        page.endText()
    }

    // Based on https://www.w3spoint.com/itext-edit-add-modify-write-an-existing-pdf-file-in-java
    private fun create() {


        val settings = Settings.get(this)
        Settings.setSignatureLocation(this, sig_location.text.toString())

        if (!settings.isValid())
            Toast.makeText(this, R.string.err_incomplete_settings, Toast.LENGTH_SHORT).show()

        // Get base PDF
        val attestation = assets.open("attestation-confinement-mars-2021.pdf").readBytes()

        val pdfReader = PdfReader(attestation)
        val out = ByteArrayOutputStream()
        val pdfStamper = PdfStamper(pdfReader, out)


        //Get the number of pages in pdf.
        val pages = pdfReader.numberOfPages
        if (pages != 2)
            throw RuntimeException("PDF has not two pages!")

        // Get the first page
        val pageOneContentBytes = pdfStamper.getOverContent(1)

        // Full name
        writeText(pageOneContentBytes, settings.name, 110F, 430F)

        // Date of birth
        writeText(pageOneContentBytes, settings.dateOfBirth, 110F, 418F)

        // Location of birth
        writeText(pageOneContentBytes, settings.placeOfBirth, 180F, 418F)

        // Address
        writeText(pageOneContentBytes, settings.address, 125F, 405F)


        // Reason
        if (workCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 60F, 296F)

        if (medicalCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 59F, 199F)

        if (familyCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 61F, 145F)

        val pageTwoContentBytes = pdfStamper.getOverContent(2)


        if (handicapCheckbox.isChecked)
            writeText(pageTwoContentBytes, "X", 59F, 759F)

        if (administrativeCheckbox.isChecked)
            writeText(pageTwoContentBytes, "X", 60F, 718F)

        if (trainCheckbox.isChecked)
            writeText(pageTwoContentBytes, "X", 62F, 649F)

        if (achatRadioButton.isChecked)
            writeText(pageTwoContentBytes, "X", 61F, 594F)

        if (demenagementButton.isChecked)
            writeText(pageTwoContentBytes, "X", 60F, 539F)

        if (demarcheAdministrativeButton.isChecked)
            writeText(pageTwoContentBytes, "X", 60F, 470F)

        if (culteButton.isChecked)
            writeText(pageTwoContentBytes, "X", 59F, 414F)

        if (manifestationsButton.isChecked)
            writeText(pageTwoContentBytes, "X", 59F, 372F)


        // Sig location
        writeText(pageTwoContentBytes, sig_location.text.toString(), 100F, 292F)

        // Sig date
        writeText(pageTwoContentBytes, sig_date.text.toString(), 80F, 278F)

        // Sig time
        writeText(pageTwoContentBytes, sig_time.text.toString(), 150F, 278F)

        // Signature
        writeText(pageTwoContentBytes, settings.name, 110F, 210F)


        pdfStamper.close()

        Log.v("CreateAttestation", "Done creating attestation.")

        val fileOut = FileOutputStream(File(filesDir, Settings.GENERATED_ATTESTATION_PDF))
        fileOut.write(out.toByteArray())
        fileOut.flush()
        fileOut.close()


        finish()
    }
}