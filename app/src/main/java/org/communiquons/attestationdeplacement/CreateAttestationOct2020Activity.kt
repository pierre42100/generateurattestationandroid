package org.communiquons.attestationdeplacement

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfContentByte
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfStamper
import kotlinx.android.synthetic.main.activity_create_attestation_oct_2020.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*

fun pad(int: Int): String {
    return int.toString().padStart(2, '0')
}

class CreateAttestationOct2020Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_attestation_oct_2020)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Pre-fill field
        sig_location.setText(Settings.getSignatureLocation(this))

        val calendar = Calendar.getInstance()
        val date =
            "${pad(calendar.get(Calendar.DAY_OF_MONTH))}/${pad(calendar.get(Calendar.MONTH) + 1)}/${pad(
                calendar.get(
                    Calendar.YEAR
                )
            )}"
        sig_date.setText(date)

        val time =
            "${pad(calendar.get(Calendar.HOUR_OF_DAY))}:${pad(calendar.get(Calendar.MINUTE))}"
        sig_time.setText(time)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_create_attestation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home)
            finish()

        if (item.itemId == R.id.action_create)
            create()

        return super.onOptionsItemSelected(item)
    }


    private fun writeText(page: PdfContentByte, text: String, x: Float, y: Float) {
        //Create BaseFont instance.
        val baseFont = BaseFont.createFont(
            BaseFont.TIMES_ROMAN,
            BaseFont.CP1252, BaseFont.NOT_EMBEDDED
        )

        page.beginText()
        page.setFontAndSize(baseFont, 14F)
        page.setTextMatrix(x, y)
        page.showText(text)
        page.endText()
    }

    // Based on https://www.w3spoint.com/itext-edit-add-modify-write-an-existing-pdf-file-in-java
    private fun create() {


        val settings = Settings.get(this)
        Settings.setSignatureLocation(this, sig_location.text.toString())

        if (!settings.isValid())
            Toast.makeText(this, R.string.err_incomplete_settings, Toast.LENGTH_SHORT).show()

        // Get base PDF
        val attestation = assets.open("attestation-oct-2020.pdf").readBytes()

        val pdfReader = PdfReader(attestation)
        val out = ByteArrayOutputStream()
        val pdfStamper = PdfStamper(pdfReader, out)


        //Get the number of pages in pdf.
        val pages = pdfReader.numberOfPages
        if (pages != 1)
            throw RuntimeException("PDF has more than one page!")

        // Get the first page
        val pageContentBytes = pdfStamper.getOverContent(1)

        // Full name
        writeText(pageContentBytes, settings.name, 110F, 627F)

        // Date of birth
        writeText(pageContentBytes, settings.dateOfBirth, 110F, 597F)

        // Location of birth
        writeText(pageContentBytes, settings.placeOfBirth, 240F, 597F)

        // Address
        writeText(pageContentBytes, settings.address, 125F, 567F)

        // Sig location
        writeText(pageContentBytes, sig_location.text.toString(), 92F, 186F)

        // Sig date
        writeText(pageContentBytes, sig_date.text.toString(), 85F, 156F)

        // Sig time
        writeText(pageContentBytes, sig_time.text.toString(), 250F, 156F)

        // Signature
        writeText(pageContentBytes, settings.name, 70F, 70F)


        // Reason
        if (workCheckbox.isChecked)
            writeText(pageContentBytes, "X", 57F, 448F)

        if (medicalCheckbox.isChecked)
            writeText(pageContentBytes, "X", 57F, 415F)

        if (familyCheckbox.isChecked)
            writeText(pageContentBytes, "X", 57F, 380F)

        if (handicapCheckbox.isChecked)
            writeText(pageContentBytes, "X", 57F, 347F)

        if (administrativeCheckbox.isChecked)
            writeText(pageContentBytes, "X", 57F, 325F)

        if (generalInterestCheckbox.isChecked)
            writeText(pageContentBytes, "X", 57F, 303F)

        if (trainCheckbox.isChecked)
            writeText(pageContentBytes, "X", 57F, 270F)

        if (petCheckBox.isChecked)
            writeText(pageContentBytes, "X", 57F, 236F)


        pdfStamper.close()

        Log.v("CreateAttestation", "Done creating attestation.")

        val fileOut = FileOutputStream(File(filesDir, Settings.GENERATED_ATTESTATION_PDF))
        fileOut.write(out.toByteArray())
        fileOut.flush()
        fileOut.close()


        finish()
    }
}