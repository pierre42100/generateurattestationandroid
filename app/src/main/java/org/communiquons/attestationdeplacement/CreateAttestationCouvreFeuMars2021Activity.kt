package org.communiquons.attestationdeplacement


import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfContentByte
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfStamper
import kotlinx.android.synthetic.main.activity_nouvelle_attestation_couvre_feu_mars_2021.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*


class CreateAttestationCouvreFeuMars2021Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nouvelle_attestation_couvre_feu_mars_2021)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Pre-fill field
        sig_location.setText(Settings.getSignatureLocation(this))

        val calendar = Calendar.getInstance()
        val date =
            "${pad(calendar.get(Calendar.DAY_OF_MONTH))}/${pad(calendar.get(Calendar.MONTH) + 1)}/${pad(
                calendar.get(
                    Calendar.YEAR
                )
            )}"
        sig_date.setText(date)

        val time =
            "${pad(calendar.get(Calendar.HOUR_OF_DAY))}:${pad(calendar.get(Calendar.MINUTE))}"
        sig_time.setText(time)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_create_attestation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home)
            finish()

        if (item.itemId == R.id.action_create)
            create()

        return super.onOptionsItemSelected(item)
    }


    private fun writeText(page: PdfContentByte, text: String, x: Float, y: Float) {
        //Create BaseFont instance.
        val baseFont = BaseFont.createFont(
            BaseFont.TIMES_ROMAN,
            BaseFont.CP1252, BaseFont.NOT_EMBEDDED
        )

        page.beginText()
        page.setFontAndSize(baseFont, 14F)
        page.setTextMatrix(x, y)
        page.showText(text)
        page.endText()
    }

    // Based on https://www.w3spoint.com/itext-edit-add-modify-write-an-existing-pdf-file-in-java
    private fun create() {


        val settings = Settings.get(this)
        Settings.setSignatureLocation(this, sig_location.text.toString())

        if (!settings.isValid())
            Toast.makeText(this, R.string.err_incomplete_settings, Toast.LENGTH_SHORT).show()

        // Get base PDF
        val attestation = assets.open("attestation-couvre-feu-mars-2021.pdf").readBytes()

        val pdfReader = PdfReader(attestation)
        val out = ByteArrayOutputStream()
        val pdfStamper = PdfStamper(pdfReader, out)


        //Get the number of pages in pdf.
        val pages = pdfReader.numberOfPages
        if (pages != 1)
            throw RuntimeException("PDF has not one page!")

        // Get the first page
        val pageOneContentBytes = pdfStamper.getOverContent(1)

        // Full name
        writeText(pageOneContentBytes, settings.name, 108F, 649F)

        // Date of birth
        writeText(pageOneContentBytes, settings.dateOfBirth, 110F, 637F)

        // Location of birth
        writeText(pageOneContentBytes, settings.placeOfBirth, 180F, 637F)

        // Address
        writeText(pageOneContentBytes, settings.address, 125F, 625F)


        // Reason
        if (workCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 58F, 548F)

        if (medicalCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 58F, 500F)

        if (familyCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 58F, 454F)

        if (handicapCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 58F, 407F)

        if (administrativeCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 58F, 373F)

        if (generalInterestCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 58F, 327F)

        if (trainCheckbox.isChecked)
            writeText(pageOneContentBytes, "X", 58F, 292F)

        if (petCheckBox.isChecked)
            writeText(pageOneContentBytes, "X", 58F, 247F)



        // Sig location
        writeText(pageOneContentBytes, sig_location.text.toString(), 100F, 212F)

        // Sig date
        writeText(pageOneContentBytes, sig_date.text.toString(), 75F, 200F)

        // Sig time
        writeText(pageOneContentBytes, sig_time.text.toString(), 150F, 200F)

        // Signature
        writeText(pageOneContentBytes, settings.name, 110F, 160F)


        pdfStamper.close()

        Log.v("CreateAttestation", "Done creating attestation.")

        val fileOut = FileOutputStream(File(filesDir, Settings.GENERATED_ATTESTATION_PDF))
        fileOut.write(out.toByteArray())
        fileOut.flush()
        fileOut.close()


        finish()
    }
}